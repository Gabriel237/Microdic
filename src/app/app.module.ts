import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingService } from '../providers/utils/loading-service';
import { IonicStorageModule } from '@ionic/storage';
import { AuthProvider } from '../providers/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AppConfigProvider } from '../providers/app-config/app-config';
import { DashboardService } from '../providers/dashboard/dashboard';
import { ListLessonPage } from '../pages/list-lesson/list-lesson';
import { GlossaireService } from '../providers/glossaire/glossaire';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { AndroidPermissions } from '@ionic-native/android-permissions';



@NgModule({
  declarations: [
    MyApp,
    // HomePage
  ],
  imports: [
    // PipesModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: false
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // HomePage
  ],
  providers: [
    // NavParams,
    GlossaireService,
    ListLessonPage,
    AppConfigProvider,
    AuthProvider,
    LoadingService,
    DashboardService,
    StatusBar,
    SplashScreen,
    FileOpener,
    File,
    FileTransfer,
    FileTransferObject,
    DocumentViewer,
    AndroidPermissions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
