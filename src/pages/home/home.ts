import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from '../../providers/utils/loading-service';
import { AuthProvider } from '../../providers/auth.service';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  formdata : FormGroup;
  errorMessages:any;
  isValidFormSubmitted = null;
  constructor(public navCtrl: NavController,
              public fb: FormBuilder,
              private loadingService: LoadingService,
              private storage: Storage,
              private authProvider: AuthProvider,
              ) {

        this.init();
  }

  ngOnInit() {
  }

  showdashboard() {
    this.navCtrl.push("DashboardPage");
  }

   private init () {
     this.formdata = this.fb.group({
       email: ["", Validators.compose([Validators.required,Validators.email])],
       password: ["", Validators.required]
     });
   }
   geterror(error?) {
      return error;
   }
  authentification(data) {
    this.loadingService.present();

    let email = this.formdata.value.email;
    let password = this.formdata.value.password;
    // console.log(this.formdata.value);
    this.authProvider.signIn(email, password).subscribe(
      (resp) => {
         if(resp) {
          console.log("response",resp);
           this.storage.set('user_saved', resp);
           this.navCtrl.push("DashboardPage");
         }
      },
      (error) => {
        this.loadingService.dismiss();
        console.log('Une erreur est survenue', JSON.parse(error.error));
        if (error.status == 400) {
          this.errorMessages = JSON.parse(error.error);
          console.log("formdata", this.errorMessages);
          return this.errorMessages;
        }
      }
    );
  }
}
