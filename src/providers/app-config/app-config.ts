import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AppConfigProvider {

  private _apiBaseURL: string = 'https://serene-sea-75593.herokuapp.com';


  constructor(public http: HttpClient) {
    console.log('Hello AppConfigProvider Provider');
  }

  apiBaseURL() {
    return this._apiBaseURL;
  }
}
