import { Injectable } from "@angular/core";
import { LoadingController, Loading } from "ionic-angular";


@Injectable()
export class LoadingService {

    load: Loading;

    constructor(public loadingCtrl: LoadingController) {

    }

    present() {
        this.load = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Patientez s\'il vous plait...',
            showBackdrop: true, // Default true
            dismissOnPageChange: true, 
            enableBackdropDismiss: false // Default true
        });

        this.load.onDidDismiss(() => {

            if (this.load) { this.load = null; }

        });

        this.load.present();
    }

    dismiss() {

        this.load.dismiss();
        
    }
}