import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap'
import * as JWT from 'jwt-decode';


@IonicPage()
@Component({
  selector: 'page-glossaire',
  templateUrl: 'glossaire.html',
})
export class GlossairePage implements OnInit {
  teaching_sequences;
  teaching_sequence;
  baseURL;
  glossaires;
  studentdata: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public appConfigProvider: AppConfigProvider,
              public httpClient: HttpClient,
              ) {
                  this.baseURL = this.appConfigProvider.apiBaseURL();
                  this.teaching_sequence = this.navParams.get('teaching_sequence')
                  console.log(this.teaching_sequence);
  }

  ngOnInit() {
    this.getglossaire().subscribe(
      resp => {
        console.log(resp)
        this.glossaires = resp;
   })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GlossairePage');
  }

  initializeItems() {
    return this.glossaires;
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  getglossaire() {
    return this.get().flatMap(token => {
      console.log(token);
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/teaching-sequences/' + this.teaching_sequence.id + '/glossaries';
      return this.httpClient.get(url, { headers: headers })
        .pipe();
    });
  }

  getItems(ev: any) {
    this.initializeItems();
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.glossaires = this.glossaires.filter((item) => {
        return (item.nom.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else {
      return this.glossaires;
    }
  }

}
