import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListLessonPage } from './list-lesson';

@NgModule({
  declarations: [
    ListLessonPage,
  ],
  imports: [
    IonicPageModule.forChild(ListLessonPage),
  ],
})
export class ListLessonPageModule {}
