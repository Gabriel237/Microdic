import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { DashboardService } from '../../providers/dashboard/dashboard';





@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {
  lessonsRestantes: any;
  note:any;
  iscertified:any;
  progress:any;
  teaching_sequences:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public httpClient: HttpClient,
              public storage: Storage,
              private dashboard: DashboardService,
              public alertCtrl: AlertController,
              public platform: Platform) {
  }

  ngOnInit() {
    // console.log(this.storage.get('user_saved'));
    this.dashboard.getlessonsRestantes().subscribe(
      resp=> {
        console.log(resp)
        this.lessonsRestantes = resp.lessonsRestantes;
      }
    )
    this.dashboard.getglobalscore().subscribe(
      resp => {
        console.log(resp)
        this.note = resp.note;
      }
    )
    this.dashboard.getcertificated().subscribe(
      resp => {
        console.log(resp)
        if (resp.iscertified == true) {
          this.iscertified = "1";
        } else {
          this.iscertified = "0";
        }
      }
    )
    this.dashboard.getprogress().subscribe(
      resp => {
        console.log(resp)
        this.progress = resp.progress;
        if (this.progress.isInteger()) {
          this.progress = this.progress;
        }
        this.progress = this.progress.toFixed(2);
      }
    )
    this.dashboard.getsequenceslessons().subscribe(
      resp => {
        console.log(resp)
        this.teaching_sequences = resp;
      }
    )

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  showlistlesson(i) {
    console.log(this.teaching_sequences[i]);
    // this.navCtrl.push('GlossairePage');
    this.navCtrl.setPages(
      [{ page: 'TeachingSequencePage', params: { "teaching_sequences": this.teaching_sequences[i] } }]
    )
  }

  log_out() {
    let alert = this.alertCtrl.create({
      title: "Deconnexion",
      subTitle: "Souhaitez vous vraiment être deconnecté?",
      buttons:[{
        text: "Oui",
        handler: data => {
          this.storage.remove('user_saved').catch(
            (error) => {
              console.log('Une erreur est survenue: ', error);
            }
          );
         this.platform.exitApp();
        }
      },{
        text: 'Non',
        handler: data => {
          console.log('Saved clicked');
        }
      }]
    });
    alert.present()
  }

}
