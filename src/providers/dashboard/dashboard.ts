import { Injectable} from '@angular/core';
import { Storage } from '@ionic/storage';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as JWT from 'jwt-decode';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap'

import { Dashboard } from '../../interface/interface.dashboard';

// var JWT=require('jwt-decode');


@Injectable()
export class DashboardService{

  isAuth = false;
  userSaved = null;
  studentdata:any;
  id:any;
  private baseURL: string;
 

  constructor(public storage: Storage,
    public appConfigProvider: AppConfigProvider,
    public httpClient: HttpClient) {
    // this.baseURL = this.appConfigProvider.apiBaseURL();
    this.init();
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  init() {
    this.storage.get('user_saved').then(
      (user) => {
        if (user) {

          this.isAuth = true;
          this.userSaved = user;
        } else {

          this.isAuth = false;
          this.userSaved = null;

        }
      },
      (error) => {
        console.log('Impossible de lire dans le Storage', error);
      }
    );
    // this.id = this.studentdata.data.id;
    // console.log(this.id)
    this.baseURL = this.appConfigProvider.apiBaseURL();
  }

  getlessonsRestantes() {
    return this.get().flatMap(token => {
      this.studentdata = JWT(token);
      console.log("token",token)
      // this.studentdata = JSON.stringify(this.studentdata)
      console.log("tokenParse",token.exp)
      // add Authorization header
      let headers = new HttpHeaders({
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/lessons/count';
      return this.httpClient.post<Dashboard>(url,null, { headers: headers })
        .pipe();
    });
  }

  getglobalscore() {
    return this.get().flatMap(token => {
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/globalscore';
      return this.httpClient.post<Dashboard>(url, null, { headers: headers })
        .pipe();
    });
  }

  getcertificated() {
    return this.get().flatMap(token => {
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/is-certificated';
      return this.httpClient.post<Dashboard>(url, null, { headers: headers })
        .pipe();
    });
  }

  getprogress() {
    return this.get().flatMap(token => {
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/progress/calc';
      return this.httpClient.post<Dashboard>(url, null, { headers: headers })
        .pipe();
    });
  }

  getsequenceslessons() {
    return this.get().flatMap(token => {
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept, Authorization",
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/teaching-sequences';
      return this.httpClient.get(url, { headers: headers })
        .pipe();
    });
  }

}
