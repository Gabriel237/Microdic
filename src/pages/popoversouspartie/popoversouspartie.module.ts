import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoversouspartiePage } from './popoversouspartie';

@NgModule({
  declarations: [
    PopoversouspartiePage,
  ],
  imports: [
    IonicPageModule.forChild(PopoversouspartiePage),
  ],
})
export class PopoversouspartiePageModule {}
