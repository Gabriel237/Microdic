import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GlossairePage } from './glossaire';

@NgModule({
  declarations: [
    GlossairePage,
  ],
  imports: [
    IonicPageModule.forChild(GlossairePage),
  ],
})
export class GlossairePageModule {}
