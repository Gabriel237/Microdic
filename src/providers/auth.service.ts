import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AppConfigProvider } from '../providers/app-config/app-config';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AuthProvider {

  isAuth = false;
  userSaved = null;


  private baseURL: string;

  constructor(public storage: Storage,
    public appConfigProvider: AppConfigProvider,
    public httpClient: HttpClient) {
    // this.baseURL = this.appConfigProvider.apiBaseURL();
    this.init();
  }

  init() {
    this.storage.get('user_saved').then(
      (user) => {
        if (user) {

          this.isAuth = true;
          this.userSaved = user;

        } else {

          this.isAuth = false;
          this.userSaved = null;

        }
      },
      (error) => {
        console.log('Impossible de lire dans le Storage', error);
      }
    );

    this.baseURL = this.appConfigProvider.apiBaseURL();
  }

  signIn(email: string, password: string){

    let data = {
      "email": email,
      "password": password
    }

    let url = this.baseURL + '/auth/students';

    return this.httpClient.post(url, data, { responseType: 'text' })
      .pipe();
  }

  signOut() {

    this.storage.remove('user_saved').catch(
      (error) => {
        console.log('Une erreur est survenue: ', error);
      }
    );

  }
}
