import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentlessonPage } from './contentlesson';

@NgModule({
  declarations: [
    ContentlessonPage,
  ],
  imports: [
    IonicPageModule.forChild(ContentlessonPage),
  ],
})
export class ContentlessonPageModule {}
