import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeachingSequencePage } from './teaching-sequence';

@NgModule({
  declarations: [
    TeachingSequencePage,
  ],
  imports: [
    IonicPageModule.forChild(TeachingSequencePage),
  ],
})
export class TeachingSequencePageModule {}
