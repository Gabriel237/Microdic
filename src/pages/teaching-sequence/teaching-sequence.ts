import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import draftToHtml from 'draftjs-to-html';

/**
 * Generated class for the TeachingSequencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teaching-sequence',
  templateUrl: 'teaching-sequence.html',
})
export class TeachingSequencePage {
  teaching_sequences: any;
  objectifs;
  situation_vie;
  evaluation_diagnostique;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.teaching_sequences = this.navParams.get('teaching_sequences');
    this.objectifs = draftToHtml(JSON.parse(this.teaching_sequences.objectifs));
    this.situation_vie = draftToHtml(JSON.parse(this.teaching_sequences.situation_vie));
    this.evaluation_diagnostique = draftToHtml(JSON.parse(this.teaching_sequences.evaluation_diagnostique));
    // console.log(this.teaching_sequences);
  }
  goback() {
    this.navCtrl.push("DashboardPage");
    console.log('Click on button Test Console Log');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TeachingSequencePage');
  }

  showlesson(){
    this.navCtrl.push('ListLessonPage', { "teaching_sequences": this.teaching_sequences});
  }

}
