import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as JWT from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import { FormBuilder, FormGroup, FormArray} from '@angular/forms';
import draftToHtml from 'draftjs-to-html';



@IonicPage()
@Component({
    selector: 'page-contentlesson',
    templateUrl: 'contentlesson.html'
})
export class ContentlessonPage implements OnInit {
    souspartie;
    lesson;
    teaching_sequence;
    studentdata;
    baseURL;
    reponses;
    id_exercice;
    exercices;
    questions;
    nberofquestion = 0;
    nberofquestionChecked = 0;
    exercice_id;
    studentExo;
    choicesend;
    pourcentage_trouver;
    contenu_souspartie;
    submit_is_valid = 0;
    note = 0;
  checked_question: any = [];
  checked_choice: any = {};
    noteTotal = {};
    choicechecked: any = [];
    Choices: any = {};
    studentChoice: any = [];
    dataReponse: FormGroup;
    datastudentChoice;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        public storage: Storage,
        public httpClient: HttpClient,
        public appConfigProvider: AppConfigProvider,
        public fb: FormBuilder) {

        this.lesson = this.navParams.get('lesson');
        this.souspartie = this.navParams.get("souspartie");
        this.teaching_sequence = this.navParams.get("teaching_sequence");
        this.baseURL = this.appConfigProvider.apiBaseURL();
        this.contenu_souspartie = draftToHtml(JSON.parse(this.souspartie.contenu));
      console.log("souspartie",this.souspartie);
      console.log("lesson",this.lesson);
      console.log("teaching_sequence",this.teaching_sequence);
    }

    ngOnInit() {
        this.dataReponse = this.fb.group({
            // checkbox_choice: ["false", Validators.required],
            // radio_id_choice: [[]],
            questionsRadio: this.fb.array([])
        });

        // this.dataReponse['studentChoice'];
        // console.log(this.getidexercice());

    }
  get questionsRadio(): FormArray {
    return this.dataReponse.get('questionsRadio') as FormArray;
  }

  setQuestions() {
    const questionFG = this.exercices.map(exercice =>
      exercice.questions.map(question => this.fb.group(new QuestionReply()))
    );
    const questionFormArray = this.fb.array(questionFG[0]);
    this.dataReponse.setControl('questionsRadio', questionFormArray);
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ContentlessonPage');
      this.getexercice().subscribe(
        resp => {
          this.exercices = resp;
          for(let i=0; i<this.exercices.length; i++) {
            this.exercice_id = this.exercices[i].id;
            this.questions = this.exercices[i].questions;
            let count = 0
            if (this.exercices[i] != this.exercices[i+1]) {
              this.noteTotal[`${this.exercices[i].id}`] = 0;
              console.log("this.exercices[i]", this.exercices[i])
            this.questions.forEach(question => {
              this.nberofquestion = this.nberofquestion + count;
              count= count + 1
              question.selected = false;
              this.noteTotal[`${this.exercices[i].id}`] = this.noteTotal[`${this.exercices[i].id}`] + question.valeur_point;
              console.log("this.noteTotal", JSON.stringify(this.noteTotal));
              question.choices.forEach(choice => {
                choice.selected = false;
              });
            });
          }
            this.get().subscribe(token => {
              // add Authorization header
              this.studentdata = JWT(token);
              let headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': token
              });
              let url = this.baseURL + '/students/' + this.studentdata.data.id + "/exercices?ids=" + `'` + this.exercice_id + `'`;
              this.httpClient.get(url, { headers: headers })
                .pipe().subscribe(
                  resp => {
                    this.reponses = resp;
                    this.reponses.forEach(reponse => {
                      this.exercices.forEach(exercice => {
                        if (reponse.id_exercice == exercice.id) {
                          this.note = reponse.note;
                          this.pourcentage_trouver = reponse.note / this.noteTotal[this.exercices[i].id];
                          exercice.questions.forEach(question => {
                            question.choices.forEach(choice => {
                              reponse.studentChoice.forEach(studentChoice => {
                                if (studentChoice.id_question == question.id && studentChoice.id_choice == choice.id && this.pourcentage_trouver >= exercice.pourcentage_reussite) {
                                  this.checked_question.push(studentChoice.id_question);
                                  this.checked_choice[`${studentChoice.id_choice}`]=studentChoice.id_choice;
                                }
                              });
                            });
                          });
                        }
                      });
                    });
                  }
                );
            });
          };//fin for exercice

          this.setQuestions();
        }
      );
    }

    showmore() {
        const popover = this.popoverCtrl.create('PopoversouspartiePage', { 'souspartie': this.souspartie, "teaching_sequence": this.teaching_sequence, "lesson": this.lesson });
        popover.present();
    }

    get() {
        return Observable.fromPromise(this.storage.get('user_saved'));
    }

    getexercice() {
        return this.get().flatMap(token => {
            // add Authorization header
            this.studentdata = JWT(token);
            let headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': token
            });
            let url = this.baseURL + '/students' + '/' + this.studentdata.data.id + '/lessons/' + this.lesson.id + '/subparts/' + this.souspartie.id + '/exercices';
            return this.httpClient.get(url, { headers: headers })
                .pipe();
        });

    }


    postreponses(data) {
        return this.get().flatMap(token => {
            // add Authorization header
            this.studentdata = JWT(token);
            let headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': token
            });
            let url = this.baseURL + '/students' + '/' + this.studentdata.data.id + '/subparts/' + this.souspartie.id + '/exercices/' + this.id_exercice + '/participer';
            return this.httpClient.post(url, data, { headers: headers })
                .pipe();
        });
    }
  select(indexExercice, i, indexChoice) {
    this.exercices.forEach(exercice => {
      if (exercice == this.exercices[indexExercice]) {
        if (this.nberofquestion == 0) {
         for(let i=0; i<exercice.questions.length; i++) {
             this.nberofquestion = this.nberofquestion + i;
         }
        }
      }
    })

    if (this.exercices[indexExercice].questions[i].is_multiple_reponse == 1) {
      if (this.exercices[indexExercice].questions[i].selected == false || this.exercices[indexExercice].questions[i].choices.every(choice => choice.selected == false) == true) {
        this.nberofquestionChecked = this.nberofquestionChecked + 1;
      }

      this.exercices[indexExercice].questions[i].selected = true ;
      this.exercices[indexExercice].questions[i].choices[indexChoice].selected = !this.exercices[indexExercice].questions[i].choices[indexChoice].selected;
      if (this.exercices[indexExercice].questions[i].choices.every(choice => choice.selected == false) == true) {
          this.nberofquestionChecked = this.nberofquestionChecked - 1;
        }
    }
    if (this.exercices[indexExercice].questions[i].is_multiple_reponse == 0) {
      if (this.exercices[indexExercice].questions[i].selected == false) {
        this.nberofquestionChecked = this.nberofquestionChecked + 1;
      }
      this.exercices[indexExercice].questions[i].selected = true;
         this.exercices[indexExercice].questions[i].choices.forEach(choice => {
           choice.selected = false;
         });
      this.exercices[indexExercice].questions[i].choices[indexChoice].selected = !this.exercices[indexExercice].questions[i].choices[indexChoice].selected;
        }
    this.id_exercice = this.exercices[indexExercice].id;
    }

    sendReponse(indexExercice) {
        let nbre = 0;
        this.note = 0;
      this.exercices[indexExercice].questions.forEach(question => {
          question.choices.forEach(choice => {
            if (choice.selected == true) {
              let studentChoices = {
                id_student: this.studentdata.data.id,
                id_question: question.id,
                id_choice: choice.id
              };
              this.studentChoice.push(studentChoices);
            }
          });
        });

      this.exercices[indexExercice].questions.forEach(question => {
            question.choices.forEach(choice => {
                if (question.is_multiple_reponse == 1) {
                    if (choice.is_right_choice == 1) {
                        nbre = nbre + question.valeur_point;
                        // this.note = this.note + question.valeur_point / nbre;
                    }
                }
                this.studentChoice.forEach(studentChoice => {
                    if (question.id == studentChoice.id_question && choice.id == studentChoice.id_choice) {
                        if (question.is_multiple_reponse == 0) {
                            if (choice.is_right_choice == 1) {
                                this.note = this.note + question.valeur_point;
                            }
                        }
                    }

                });
            });
        });

      this.exercices[indexExercice].questions.forEach(question => {
            question.choices.forEach(choice => {
                this.studentChoice.forEach(studentChoice => {
                    if (question.id == studentChoice.id_question && choice.id == studentChoice.id_choice) {
                        if (question.is_multiple_reponse == 1) {
                            if (choice.is_right_choice == 1) {
                                this.note = this.note + question.valeur_point / nbre;
                            }
                        }
                    }

                });
            });
        });


        let studentExo = {
            id_student: this.studentdata.data.id,
            id_exercice: this.id_exercice,
            note: this.note
        };

      this.pourcentage_trouver = this.note / this.noteTotal[this.exercices[indexExercice].id];

        let dataReponse = { studentExo, studentChoice: this.studentChoice };
        this.postreponses(dataReponse).subscribe(resp => {
            resp[0].studentChoice.forEach(studentChoice => {
                this.choicesend = studentChoice.id_choice;
            });
        });

    }
  terminer() {
    this.get().subscribe(token => {
      // add Authorization header
      this.studentdata = JWT(token);
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students' + '/' + this.studentdata.data.id + '/teaching-sequences/' + this.teaching_sequence.id + '/lessons/' + this.lesson.id + '/subparts/' + this.souspartie.id + '/terminer';
      this.httpClient.patch(url, {}, { headers: headers })
        .pipe().subscribe(resp => {
          this.navCtrl.pop();
          console.log("resp", resp);
        });
    });
  }

}

class QuestionReply {
  radio_id_choice: number;
  checkbox_choice: number;

  constructor() {
    this.radio_id_choice = null;
    this.checkbox_choice = null;
  }

}
