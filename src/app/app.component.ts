import { Component, OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { File,} from '@ionic-native/file';

// import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  rootPage:any = "HomePage";
  isAuth: boolean = false;
  dirs;
  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private storage: Storage,
              private file: File,) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      if(this.dirs == null) {
        this.file.createDir(this.file.externalRootDirectory, 'Microdic', true).then(
          (dir) => {
            this.dirs = dir.toURL();
            this.file.createDir(this.dirs, 'Download', true).then(
              (dir1) => {
              });
          });

      }
    });

  }

  ngOnInit(){
    this.init();
  }

  private init() {

    this.storage.get('user_saved').then(
      (user) => {
        if (user) {

          this.rootPage = "DashboardPage";

        } else {

          this.rootPage = 'HomePage';

        }
      },
      (error) => {
        console.log('Impossible de lire dans le Storage', error);
      }
    );
  }
}

