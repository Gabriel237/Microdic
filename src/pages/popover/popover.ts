import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {
  teaching_sequence;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.teaching_sequence = this.navParams.get('teaching_sequence')
    console.log(this.teaching_sequence);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  showglossaire() {
    this.navCtrl.push("GlossairePage", { 'teaching_sequence': this.teaching_sequence });
  }

}
