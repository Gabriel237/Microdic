import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, App } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import 'rxjs/add/operator/mergeMap'
import { Storage } from '@ionic/storage';
import * as JWT from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { ListLesson } from '../../interface/interface.list_lesson';

/**
 * Generated class for the ListLessonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-list-lesson',
  templateUrl: 'list-lesson.html',
})
export class ListLessonPage implements OnInit{
  teaching_sequences:any;
  baseURL:any;
  isAuth = false;
  Urlimage = 'http://192.168.8.1:5000/lessons'
  userSaved : string;
  private navCtrl: NavController;
  studentdata;
  list_lessons;

  constructor(private app: App,
              public navParams: NavParams,
              public httpClient:HttpClient,
              public storage: Storage,
              public appConfigProvider: AppConfigProvider,
              public popoverCtrl: PopoverController) {
    this.baseURL = this.appConfigProvider.apiBaseURL();
    this.navCtrl = this.app.getActiveNav();
    this.teaching_sequences = this.navParams.get('teaching_sequences')
    console.log(this.teaching_sequences);
  }

  ngOnInit() {
    // console.log(this.get());
    this.get_list_lesson().subscribe(resp =>
      {
        console.log(resp);
        this.list_lessons = resp.lessons;
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListLessonPage');
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  get_teaching_sequence() {
    return this.teaching_sequences
  }

  get_list_lesson() {
    return this.get().flatMap(token => {
        // add Authorization header
      this.studentdata = JWT(token);
        let headers = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': token
        });
      let url = this.baseURL + '/students' + '/' + this.studentdata.data.id + '/teaching-sequences/' + this.teaching_sequences.id+'/lessons';
      return this.httpClient.get<ListLesson>(url, {headers:headers})
      .pipe();
    });

  }

  showlesson(i) {
    this.navCtrl.push("LessonPage", { 'teaching_sequence': this.teaching_sequences, 'lesson': this.list_lessons[i]});
  }

  showmore() {
    const popover = this.popoverCtrl.create('PopoverPage', { 'teaching_sequence': this.teaching_sequences});
    popover.present();
  }

}
