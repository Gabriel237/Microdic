import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import * as JWT from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/mergeMap'
import { FileTransferObject } from '@ionic-native/file-transfer';
import { File, FileEntry } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
declare var require: any


var fileDownload = require('js-file-download');
@IonicPage()
@Component({
  selector: 'page-popoversouspartie',
  templateUrl: 'popoversouspartie.html',
})
export class PopoversouspartiePage implements OnInit{
  studentdata;
  souspartie;
  lesson;
  medias;
  baseURL;
  i;
  error;
  entry;
  dirs;
  teaching_sequence;
  fileTransfer: FileTransferObject;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              private httpClient: HttpClient,
              public appConfigProvider: AppConfigProvider,
              private file: File,
              public viewCtrl: ViewController,
              private fileOpener: FileOpener) {
    this.baseURL = this.appConfigProvider.apiBaseURL();
    this.lesson = this.navParams.get("lesson");
    this.souspartie = this.navParams.get("souspartie");
    this.teaching_sequence = this.navParams.get("teaching_sequence")
    console.log(this.souspartie);
    console.log(this.lesson);
    console.log(this.teaching_sequence);
  }
  ngOnInit() {
    this.getmedias().subscribe(
      resp => {
        console.log(resp);
        this.medias = resp;
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoversouspartiePage');
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  getmedias() {
    return this.get().flatMap(token => {
      // add Authorization header
      this.studentdata = JWT(token);
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students' + '/' + this.studentdata.data.id +
        '/teaching-sequences/' + this.teaching_sequence.id + '/lessons/' + this.lesson.id +
        '/parts/' + this.souspartie.id_partie_lesson + '/subparts/' + this.souspartie.id + '/medias';
      return this.httpClient.get(url, { headers: headers })
        .pipe();
    });
  }

  getmediaid(i){
    // console.log(this.medias[i]);
    return this.get().flatMap(token => {
      // add Authorization header
      this.studentdata = JWT(token);
      let headers = new HttpHeaders({
        'Authorization': token
      });
      let url = this.baseURL + '/students' + '/' + this.studentdata.data.id +
        '/teaching-sequences/' + this.teaching_sequence.id + '/lessons/' + this.lesson.id +
        '/parts/' + this.souspartie.id_partie_lesson + '/subparts/' + this.souspartie.id + '/medias/' + this.medias[i].id;
      return this.httpClient.get(url, { headers: headers, responseType:'blob'})
        .pipe();
    });
  }
  getmedia(i) {
    // var fileDownload = require('js-file-download');

    this.getmediaid(i).subscribe(resp=>{
      console.log(resp)
      fileDownload(resp, this.medias[i].timestamp);
      this.file.writeFile(this.file.externalRootDirectory + "Microdic/Download/", this.medias[i].titre + this.medias[i].extension, resp, { replace: true }).then((fileEntry: FileEntry) => {

        console.log("File created!");
        this.entry = decodeURI(fileEntry.toURL());
        //Open with File Opener plugin
        this.fileOpener.open(decodeURI(fileEntry.toURL()), resp.type)
          .then(() => console.log('File is opened'))
          .catch(err => {
            this.error = "error Opener: " + err;
            console.error('Error openening file: ' + err)});
      })
        .catch((err) => {
          this.error = err;
          console.error("Error creating file: " + err);
          throw err;  //Rethrow - will be caught by caller
        });
    })
    this.viewCtrl.dismiss();
  }


}
