import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/mergeMap'
import { Storage } from '@ionic/storage';
import * as JWT from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import { Lesson } from '../../interface/interface.lesson';
import draftToHtml from 'draftjs-to-html';

@IonicPage()
@Component({
  selector: 'page-lesson',
  templateUrl: 'lesson.html',
})
export class LessonPage implements OnInit{
  lesson;
  teaching_sequences;
  studentdata;
  num_lesson;
  objetif_lesson;
  baseURL;
  Parties;
  sousparties:any =[];
  toggle: boolean = true;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              private httpClient: HttpClient,
              public appConfigProvider: AppConfigProvider) {

              this.baseURL = this.appConfigProvider.apiBaseURL();
              this.lesson = this.navParams.get('lesson');
              this.teaching_sequences = this.navParams.get('teaching_sequence');
              this.num_lesson = this.lesson.numero;
              this.objetif_lesson = draftToHtml(JSON.parse(this.lesson.objectifs));
              console.log(this.lesson);
              console.log(this.teaching_sequences);
  }

  ngOnInit() {
    this.getlesson().subscribe(resp => {
      console.log(resp);
      this.Parties = resp.planning;
      for(let i=0; i<this.Parties.length; i++) {
        for (let j = 0; j < this.Parties[i].chapters.length; j++) {
          this.sousparties.push(this.Parties[i].chapters[j]);
        }
      }
    });
    console.log(this.sousparties);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LessonPage');
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  getlesson() {
    return this.get().flatMap(token => {
      // add Authorization header
      this.studentdata = JWT(token);
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students' + '/' + this.studentdata.data.id +
        '/teaching-sequences/' + this.teaching_sequences.id + '/lessons/' + this.lesson.id + '/planning-course' ;
      return this.httpClient.get<Lesson>(url, { headers: headers })
        .pipe();
    });

  }

  showlesson(i) {
    console.log(this.sousparties[i]);
    this.navCtrl.push("ContentlessonPage", { "souspartie": this.sousparties[i], "teaching_sequence": this.teaching_sequences,"lesson":this.lesson});
  }
}
