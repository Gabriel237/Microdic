import { Injectable} from '@angular/core';
import { Storage } from '@ionic/storage';
import { AppConfigProvider } from '../../providers/app-config/app-config';
import { HttpHeaders, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap'
import * as JWT from 'jwt-decode';
import { ListLessonPage } from '../../pages/list-lesson/list-lesson';


@Injectable()
export class GlossaireService {

  isAuth = false;
  userSaved = null;
  teaching_sequence;
  studentdata: any;
  id: any;
  private baseURL: string;
  constructor(public storage: Storage,
              public appConfigProvider: AppConfigProvider,
              public httpClient: HttpClient,
              public teaching_sequences:ListLessonPage) {
    this.baseURL = this.appConfigProvider.apiBaseURL();
    this.teaching_sequence = this.teaching_sequences.get_teaching_sequence();
  }

  get() {
    return Observable.fromPromise(this.storage.get('user_saved'));
  }

  getglossaire() {
    return this.get().flatMap(token => {
      console.log(token);
      this.studentdata = JWT(token);
      // add Authorization header
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      let url = this.baseURL + '/students/' + this.studentdata.data.id + '/teaching-sequences/' + this.teaching_sequence.id + '/glossaries';
      return this.httpClient.get(url, { headers: headers })
        .pipe();
    });
  }

}
